var translation = {
  menu: function(ev, sections, admin_href, admin_name, hash) {
    if (checkEvent(ev)) return true;

    var inline_translation = translation.enabled() ? 'Disable inline translation' : 'Enable inline translation';
    var show_all_phrases = '', invitation = '';
    var section_id = (sections || '').split(',');
    section_id = section_id[0] || 0;
    if (section_id) {
      show_all_phrases = '<div class="flat_button secondary" id="translation_show_all">Show all phrases</div>';
    }
    if (admin_href == 'super') {
      invitation = '<div>You are super user.<br><a href="/translation.php?act=translators">Add translators &raquo;</a></div>';
    } else if (admin_href == 'coordinator') {
      invitation = '<div>You are coordinator.<br><a href="/translation.php?act=translators">Add translators &raquo;</a></div>';
    } else if (admin_href) {
      invitation = '<a href="' + admin_href + '">' + admin_name + '</a> has invited you to translate this page. <a onclick="return translation.invite();">Invite friend &raquo;</a>';
    }

    if (invitation) {
      invitation = '\
        <div id="translation_inv_wrap">\
          <div id="translation_inv_text">' + invitation + '</div>\
          <div id="translation_inv_box">\
            <div class="progress" id="translation_inv_progress"></div>\
            <table cellspacing="0" cellpadding="0" id="translation_inv_controls">\
              <tr>\
                <td><input type="hidden" class="text" id="translation_inv_id" name="translation_inv_id" /></td>\
                <td style="padding-left: 6px"><a onclick="translation.doInvite(\'' + hash + '\')">Send</a></td>\
              </tr>\
            </table>\
          </div>\
        </div>';
    }

    var box = showFastBox({
      title: 'Select option',
      width: 300,
      bodyStyle: 'padding: 0px',
      dark: 1,
      flatButtons: true,
      onClean: function() {
        cleanElems('translation_toggle', 'translation_to_page', 'translation_show_all');
        if (translation.uiFriends) {
          translation.uiFriends.destroy();
          translation.uiFriends = false;
        }
      }
    }, invitation + '\
      <div class="translation_box">\
        <div class="button_blue flat_button" id="translation_toggle">' + inline_translation + '</div>\
        ' + show_all_phrases + '\
        <a class="button_link" href="/translation.php?section_id=' + section_id + '">\
          <div class="flat_button secondary" id="translation_to_page">Go to translation page</div>\
        </a>\
        <div class="help">\
          <a href="/club16000">Help &raquo;</a>\
          <br>\
          <a href="/translation.php?section_id=untraslated">Untranslated &raquo;</a>\
        </div>\
      </div>');
    ge('translation_toggle').onclick = translation.toggle;
    ge('translation_to_page').onclick = function() {};
    if (section_id) {
      ge('translation_show_all').onclick = translation.showAll.pbind(sections, box);
    }
    return false;
  },
  save: function(box) {
    if (isVisible(box.progress)) return;

    var query = serializeForm(ge('translation_form'));
    if (nav.strLoc.indexOf('translation.php') != -1) {
      query.truncate = 1;
    }
    ajax.post('translation.php', query, {onDone: function(t) {
      box.el.innerHTML = t;
      box.el.className = 'translated';
      box.hide();
    }, onFail: function(t) {
      if (t) {
        var error = ge('error');
        error.innerHTML = t;
        show(error);
        return true;
      }
    }, progress: box.progress});
  },
  update: function(box) {
    var nativeText = ge('native_text'), history = ge('historyWrap');
    var items = geByTag('textarea', ge('translation_form')), tokens;
    if (getSize(nativeText)[1] > 160) {
      setStyle(nativeText, {height: '160px', overflow: 'auto'});
    } else {
      setStyle(nativeText, {height: 'auto', overflow: 'visible'});
    }

    var options = ge('inlineAdditional'), opVisible = isVisible(options);
    if (!opVisible) show(options);
    if (history) {
      if (getSize(history)[1] > 160) {
        setStyle(history, {height: '160px', overflow: 'auto'});
      }
    }
    each(items, function() {
      autosizeSetup(this, {height: 180});
      addEvent(this, 'keydown', function(e) {
        if ((e.ctrlKey || e.metaKey) && e.keyCode == 13) {
          translation.save(box);
        }
      });
      tokens = ge('tokens_' + this.id.substr(9));
      if (tokens && tokens.childNodes.length) {
        AutoTokens(this, tokens);
      } else if (tokens) {
        hide(tokens);
      }
    });
    if (!opVisible) hide(options);
  },
  invite: function() {
    var inviteBox = ge('translation_inv_box');
    hide('translation_inv_text');
    show(inviteBox);
    if (!translation.uiFriends) {
      ajax.post('friends_ajax.php', {from: 'inline', filter: 'tiny'}, {onDone: function(response) {
        var result = eval('(' + response + ')');
        if (result.friends && result.friends.length) {
          stManager.add(['ui_controls.css', 'ui_controls.js'], function() {
            translation.uiFriends = new Dropdown(ge('translation_inv_id'), result.friends, {width: 150, autocomplete: true, placeholder: 'Start typing friend\'s name'});
            show('translation_inv_controls');
          });
          return;
        }
        var invitationText = ge('translation_inv_text');
        invitationText.innerHTML = '<div style="text-align:center;padding-top:8px">You have no friends.</div>';
        hide('translation_inv_box');
        show(invitationText);
      }, progress: 'translation_inv_progress', stat: ['ui_controls.css', 'ui_controls.js']});
    } else {
      hide('translation_inv_progress');
      show('translation_inv_controls');
      translation.uiFriends.clear();
    }
  },
  doInvite: function(hash) {
    var fid = translation.uiFriends.val();
    if (!fid) return;
    hide('translation_inv_controls');
    ajax.post('translation.php', {act: 'a_invite_translator', user_id: fid, hash: hash}, {onDone: function(text) {
      var invitationText = ge('translation_inv_text');
      invitationText.innerHTML = '<div style="text-align: center">' + text + '<br><a onclick="translation.invite();">Go back</a></div>';
      hide('translation_inv_box');
      show(invitationText);
    }, progress: 'translation_inv_progress'});
  },
  cookie_key: 'remixinline_trans',
  toggle: function() {
    setCookie(translation.cookie_key, translation.enabled() ? '' : '1', 360);
    nav.reload({force: true});
  },
  enabled: function() {
    return getCookie(translation.cookie_key) ? true : false;
  },
  showAll: function(sections, box) {
    if (!sections) return;
    var el = ge('translation_all') || geByClass1('scroll_fix', pageNode).appendChild(ce('div', {className: 'clear', id: 'translation_all'}));
    el.innerHTML = '<div class="loading">Loading...</div>';
    show(el);
    box.hide(200);
    var coords = getXY(el);
    if (browser.msie6) {
      animate(pageNode, {scrollTop: coords[1]});
    } else {
      animate(htmlNode, {scrollTop: coords[1]});
      animate(bodyNode, {scrollTop: coords[1]});
    }
    ajax.post('translation.php', {act: 'inline_edit_all', sections: sections}, {onDone: function(text) {
      el.innerHTML = text;
    }});
  },
  hideAll: function() {
    var el = ge('translation_all');
    if (!el) return;
    el.innerHTML = '';
    hide(el);
    scrollToTop();
  },

  _updateKeysList: function(sectionId, keysHtml, untranslatedCounts, totalUntranslatedCount) {
    translation._progress = false;
    hide('progress-spinner');

    ge("key-rows").innerHTML = keysHtml;

    // clear current badges
    var badges = geByClass("untranslated-badge");
    for (var i = 0; i < badges.length; i++) {
      badges[i].innerHTML = '';
      hide(badges[i]);
    }

    untranslatedCounts = JSON.parse(untranslatedCounts);
    for(var id in untranslatedCounts) {
      if (untranslatedCounts.hasOwnProperty(id)) {
        var count = untranslatedCounts[id][1];
        var badgeEl = geByClass1("untranslated-badge", ge("section_" + id));
        badgeEl.innerHTML = count;
        toggle(badgeEl, count > 0);
      }
    }

    var badgeEl = geByClass1("untranslated-badge", ge("section_untranslated"));
    badgeEl.innerHTML = totalUntranslatedCount;
    toggle(badgeEl, totalUntranslatedCount > 0);

    var sectionTotal = untranslatedCounts.length ? untranslatedCounts[sectionId][1] : 0;
    if (sectionTotal > 0) {
      ge("keys-summary-text").innerHTML = langNumeric(sectionTotal, cur.lang.tran_untranslated_keys_summary);
    } else {
      ge("keys-summary-text").innerHTML = getLang('tran_no_untranslated_keys_summary');
    }
  },

  initKeysTab: function(languagesList) {
    var self = this;
    var searchInput = ge('quick-search-input');

    addEvent(searchInput, 'change keypress', function(event) {
      if (event.type == 'keypress' && event.keyCode != 13) return;

      var searchStr = val(searchInput);

      if (!searchStr) {
        self.selectSection();
        return;
      }

      if (translation._progress) return;
      translation._progress = true;

      translation._selectSectionItemUI();
      scrollToTop();

      ge('keys-summary-text').innerHTML = '';
      show('progress-spinner');
      ajax.post('al_translation.php', {act: 'get_section_keys', section_id: 'search', search_str: searchStr, lang_id: nav.objLoc.lang_id }, {
        onDone: self._updateKeysList.bind(this)
      });
    });

    var languageSelect = new MDropdown(ge('lang-chooser'), {
      items: languagesList,
      width: 220,
      onSelect: function(langId) {
        self.selectSection(null, langId);
      }
    });
  },

  _selectSectionItemUI: function(sectionId) {
    var selectedSections = geByClass("selected", geByClass1("keys-menu"));
    for (var i = 0; i < selectedSections.length; i++) {
      removeClass(selectedSections[i], 'selected');
    }

    addClass("section_" + sectionId, "selected");

    function isNumber(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }

    if (isNumber(sectionId)) {
      ls.set('tran_last_sectionid', sectionId);
    }

    if (sectionId)
      val(ge('quick-search-input'), '');
  },

  selectSection: function(sectionId, langId) {
    if (translation._progress) return;

    translation._progress = true;
    var self = this;

    var location = {};
    var searchStr;

    if (sectionId === '' || sectionId == undefined) {
      searchStr = trim(val(ge('quick-search-input')));
      if (searchStr) {
        location.search_str = searchStr;
        sectionId = 'search';
      } else {
        sectionId = nav.objLoc.section_id || 0;
      }
    }

    location.section_id = sectionId;

    if (langId || nav.objLoc.lang_id) {
      location.lang_id = langId || nav.objLoc.lang_id;
    }

    this._selectSectionItemUI(sectionId);

    if (sectionId == 'search') {
      delete location.section_id;
      delete location.search_str;
      nav.setLoc(extend(nav.objLoc, location));
      location.section_id = sectionId;
      location.search_str = searchStr;
    } else {
      nav.setLoc(extend(nav.objLoc, location));
    }
    scrollToTop();

    ge('keys-summary-text').innerHTML = '';
    show('progress-spinner');
    ajax.post('al_translation.php', extend({act: 'get_section_keys'}, location), {
      onDone: self._updateKeysList.bind(this)
    });
  },

  t: function(ev, el, leftBtn) {
    if (ev.type == 'click' && (!ev.altKey && !leftBtn)) return;
    var langKey = (el.id.substr(0, 5) == 'lang_') ? el.id.substr(5) : el.id;
    //var box = showBox('translation.php', {act: 'inline', key: langKey}, {params: {width: 490}, stat: ['ui_controls.css', 'ui_controls.js']});
    //box.el = el;
    showBox('al_translation.php', {act: 'edit_key_dialog', key: langKey}, {params: {width: 600, dark: true, bodyStyle: 'padding: 0', flatButtons: true, forceNoBtn: true}, stat: ['ui_controls.css', 'ui_controls.js']});
    return cancelEvent(ev);
  },

  editKey: function(key) {

    showBox('al_translation.php', {act: 'edit_key_dialog', key: key, section_id: nav.objLoc.section_id, lang_id: nav.objLoc.lang_id }, {params: {dark: true, bodyStyle: 'padding: 0', width: 600, flatButtons: true, buttonsAtLeft: true, forceNoBtn: true}, noreload: true});
  },

  _getTextareaValueIndex: function(id) {
    return id.split('-')[2];
  },

  _getTextareaLanguageId: function(id) {
    return id.split('-')[1];
  },

  _initTextareasSMSCounter: function () {
    function _updateSMSCounter(textarea) {
      ge('sms-count-text').innerHTML = 'SMS count: ' + translation._getSMSCount(val(textarea));
    }
    var textareas = geByTag("textarea");
    for (var i = 0; i < textareas.length; i++) {
      var ta = textareas[i];
      if (this._getTextareaValueIndex(ta.id) == 0) {
        removeEvent(ta, 'keyup input');
        addEvent(ta, 'keyup input', function(event) {
          _updateSMSCounter(event.target);
        });
        _updateSMSCounter(ta);
      }
    }
  },

  _initAutotokens: function (dialogBody) {
    function _autoTokens(textarea, tokens) {
      var timeout, pattern = /{[^}]+}|%[a-z]/gi;
      addEvent(textarea, 'keyup', function() {
        clearTimeout(timeout);
        setTimeout(function(){
          onEnterText();
        }, 200);
      });
      each(tokens.childNodes, function(i, x){
        if (x.nodeType == 1) {
          addEvent(x, 'click', function(){return insertToken(x, x.innerHTML)});
        }
      });
      function onEnterText(e) {
        var matches = textarea.value.match(pattern);
        var index, token;
        each (tokens.childNodes, function(i, x) {
          if (x.nodeType != 1)
            return;
          token = x.innerHTML;
          index = matches != null ? indexOf(matches, token) : -1;
          if (index != -1) {
            matches.splice(index, 1);
            x.style.display = 'none';
          } else {
            x.style.display = '';
          }
        });
      }
      function insertToken(el, token) {
        if (textarea.selectionStart != textarea.selectionEnd) {
          textarea.value += token;
        } else {
          var sel = textarea.selectionStart + token.length;
          textarea.value = textarea.value.substring(0, textarea.selectionStart) + token + textarea.value.substr(textarea.selectionStart);
        }
        hide(el);
        if (sel > 0) {
          textarea.selectionStart = textarea.selectionEnd = sel;
        }
        textarea.focus();
        return false;
      }
    }

    each(geByTag("textarea", dialogBody), function(i, ta) {
      if (!hasClass(ta, 'code-font')) return;

      var tokensEl = ge('tokens_' + translation._getTextareaValueIndex(ta.id));
      if (tokensEl && tokensEl.childNodes.length) {
        _autoTokens(ta, tokensEl);
      }
    });
  },

  _initTextareasAutofill: function (dialogBody) {
    var textareas = geByTag("textarea", dialogBody);

    for (var i = 0; i < textareas.length; i++) {
      var ta = textareas[i];
      if (!hasClass(ta, 'code-font')) continue;

      if (translation._getTextareaLanguageId(ta.id) == 0) { // rus translation
        removeEvent(ta, 'keyup input');
        addEvent(ta, 'keyup input', function(event) {
          var index = translation._getTextareaValueIndex(event.target.id);
          for (var i = 0; i < textareas.length; i++) {
            var ta = textareas[i];
            var index2 = translation._getTextareaValueIndex(ta.id);
            var langId = translation._getTextareaLanguageId(ta.id);
            if (langId == 3 && index == index2 && ta.getAttribute('data-do-not-change') != '1') {
              val(ta, val(event.target));
            }
          }
        });
      } else {
        removeEvent(ta, 'keypress');
        addEvent(ta, 'keypress', function(event) {
          event.target.setAttribute('data-do-not-change', '1');
        });
      }
    }
  },

  initEditKeyDialog: function(key, sectionId, hash, sectionsList, isDeletedKey) {
    var dialogBody = curBox().bodyNode;

    if (isNaN(sectionId)) {
      sectionId = 0;
    }

    var isAddingNewKey = !key;
    var newKeySectionDropdown;

    if (isAddingNewKey) {
      var input = ge('sections');
      input.value = sectionId;

      var keyInput = ge('key');

      function _onSectionSelect(sectionId) {
        var prefix = '';
        for (var i = 0; i < sectionsList.length; i++) {
          if (sectionsList[i][0] == sectionId) {
            prefix = sectionsList[i][3];
            break;
          }
        }

        if (isArray(prefix)) prefix = prefix[0];

        var curValue = val(ge('key'));
        var tokens = curValue.split('_');
        tokens.shift();
        curValue = tokens.join('_');
        val(keyInput, prefix + '_' + curValue);
      }

      function _onKeyInputChange() {
        var key = val(keyInput);
        if (!key) return;

        var sectionPrefix = key.split('_')[0];
        if (sectionPrefix) {
          for (var i = 0; i < sectionsList.length; i++) {
            if (sectionsList[i][3] == sectionPrefix) {
              newKeySectionDropdown.val(sectionsList[i][0]);
              break;
            }
          }
        }
      }

      newKeySectionDropdown = new Dropdown(input, sectionsList, {
        width: 208,
        onChange: _onSectionSelect
      });

      addEvent(keyInput, 'input change', _onKeyInputChange);

      _onSectionSelect(sectionId);

      translation._initTextareasAutofill(dialogBody);
    } else {
      var toggleLink = geByClass1('ru_en_toggle', bodyNode);
      addEvent(toggleLink, 'click', function() {
        toggle('ru_en_phrases_wrap');
        toggleLink.innerHTML = isVisible('ru_en_phrases_wrap') ? toggleLink.getAttribute("data-hide-text") : toggleLink.getAttribute("data-show-text");
      });

      var descriptionInfo = ge('row_description_info');
      if (!geByClass1('value', descriptionInfo).innerHTML) {
        re(descriptionInfo);
      }

      var toggleBtn = ge('toggle_history_and_parameters');
      addEvent(toggleBtn, 'click', function() {
        slideToggle('history_and_parameters', 100);
      });

      // auto height textareas
      var textareas = geByClass("code-font", dialogBody);
      for (var i = 0; i < textareas.length; i++) {
        var ta = textareas[i], txt = val(ta);
        if (txt) {
          var taSize = getSize(ta);
          txt = txt.replace(/(?:\r\n|\r|\n)/g, '<br />');
          var temp = ce("div", { innerHTML: txt, className: "text code-font" });
          dialogBody.appendChild(temp);
          setStyle(temp, { 'width': taSize[0] });
          var size = getSize(temp);
          re(temp);
          setStyle(ta, 'height', Math.min(size[1], 200) + 5);
        } else {
          setStyle(ta, 'height', 20);
        }
      }

      translation._initAutotokens(dialogBody);
    }

    // Function types
    var functionTypeDD;
    function _onFunctionTypeChange(functionType) {
      var query, translationFields;

      if (isAddingNewKey) {
        query = serializeForm(ge('section-ru-translation'));
      } else {
        query = serializeForm(ge('section-translation'));
      }
      query.act = 'inline_fields';
      query.function_type = functionType;
      query.adding_new_key = isAddingNewKey;

      show('dialog-progress-spinner');

      re('function_type_dd');
      ajax.post('al_translation.php', query, {
        onDone: function (html1, html2) {
          if (html2) {
            ge('section-ru-translation').innerHTML = html1;
            ge('section-en-translation').innerHTML = html2;
          } else {
            ge('section-translation').innerHTML = html1;
          }

          translation._initTextareasAutofill(dialogBody);

          _initFunctionTypeDropdown();

          hide('dialog-progress-spinner');
        }
      });
    }
    function _initFunctionTypeDropdown() {
      functionTypeDD = ge('function_type_dd');
      if (functionTypeDD) {
        functionTypeDD = new MDropdown(functionTypeDD, {
          width: 175,
          //rtl: true,
          onSelect: _onFunctionTypeChange
        });
      }
    }
    _initFunctionTypeDropdown();

    // Extended controls
    var disableInlineCheckbox = new Checkbox(ge('disable_inline'), {label: 'Disable inline translating'});
    var extendedWikiCheckbox = new Checkbox(ge('extended_wiki'), {label: 'Extended Wiki markup', onChange: function(value) {
      var el = geByClass1('extended-wiki-enabled');
      if (value) {
        addClass(el, 'enabled');
      } else {
        removeClass(el, 'enabled');
      }
    }});
    var hasCaseCheckbox = new Checkbox(ge('has_case'), { label: 'Depends on case', onChange: function(value) {
      toggle('translation-case', !!value);
    }});
    var markUntranslatedCheckbox = new Checkbox(ge('mark_untranslated'), { label: 'Mark as untranslated'});

    // Case controls ...
    input = ge('case');
    if (input) {
      var caseDropdown = new Dropdown(input, eval(input.getAttribute('data-types')), {
        width: 208,
        onChange: function(value) {
        }
      });
      input = ge('case_token');
      var tokens = eval(input.getAttribute('data-tokens-list'));
      var caseTokenDropdown = new Dropdown(input, tokens || [], {
        width: 208,
        onChange: function(value) {
        }
      });
    }

    if (!isAddingNewKey && !isDeletedKey) {
      this._initActions(key, sectionId, hash, sectionsList);
    }

    if (sectionId == 37) {
      translation._initTextareasSMSCounter();
    }

    if (isAddingNewKey) {
      elfocus('key');
    } else {
      var ta = geByTag("textarea", dialogBody);
      if (ta && ta[0]) ta[0].select();
    }

    curBox().removeButtons().addButton(getLang('box_cancel'), function() {
      curBox().hide();
    }, "no");

    if (isDeletedKey) {
      curBox().addButton(getLang('tran_restore_key_button_label'), function(button) {
        var query = serializeForm(geByClass1('edit-sections-wrap'));
        query.act = "restore_key";
        query.key = key;

        hide('key-save-error');
        lockButton(button);

        ajax.post('al_translation.php', query, {
          onDone: function(error) {
            unlockButton(button);
            if (error) {
              ge('key-save-error').innerHTML = error;
              show('key-save-error');
            } else {
              curBox().hide();
              translation.selectSection();
            }
          }
        });
      });
    } else {
      // Init shortkey for Save
      function _onDialogKeyPress(event) {
        if ((event.ctrlKey || event.metaKey) && event.which == 13) {
          var button = geByTag1('button', geByClass1('box_controls')); // very hacky
          _onSave(geByTag1('button', geByClass1('box_controls')), function(savedKey) {
            var rowEl = ge(savedKey).nextSibling;
            while (rowEl && hasClass(rowEl, 'translated')) {
              rowEl = rowEl.nextSibling;
            }
            if (rowEl) {
              translation.editKey(rowEl.getAttribute('id'));
            } else {
              // update keys list
              translation.selectSection();
            }
          });
        }
      }
      addEvent(curBox().bodyNode, 'keydown', _onDialogKeyPress);

      function _onSave(button, callback) {
        var query = serializeForm(geByClass1('edit-sections-wrap'));
        query.act = "save_key";
        query.lang_id = nav.objLoc.lang_id;
        if (isAddingNewKey) {
          query.is_new_key = true;
          query.section_id = newKeySectionDropdown.val();
        } else {
          query.key = key;
        }
        hide('key-save-error');
        lockButton(button);
        ajax.post('al_translation.php', query, {
          onDone: function(error) {
            unlockButton(button);
            if (error) {
              ge('key-save-error').innerHTML = error;
              show('key-save-error');
            } else {
              curBox().hide();
              if (callback && isFunction(callback)) {
                callback(query.key);
              } else {
                translation.selectSection();
              }
            }
          }
        });
      }

      curBox().addButton(isAddingNewKey ? getLang('tran_create_key') : getLang('box_save'), _onSave);
    }
  },

  _initActions: function(key, sectionId, hash, sectionsList) {
    var keyWOPrefix = key.split('_');
    keyWOPrefix.shift();
    keyWOPrefix = keyWOPrefix.join('_');

    function selectAction(actionId) {
      hide('action_delete_controls', 'action_move_controls', 'action_clone_controls');

      if (actionId == 0) {
        show('action_delete_controls');
      } else if (actionId == 1) {
        show('action_clone_controls');
      } else if (actionId == 2) {
        show('action_move_controls');
      }
    }

    function onSectionChange(newSectionId) {
      if (newSectionId == -1) {
        ge('action_clone_prefix').innerHTML = ge('action_move_prefix').innerHTML = '';
        ge('clone_key_name').value = ge('move_key_name').value = '';
        ge('clone_key_name').disabled = ge('move_key_name').disabled = true;
        return;
      }

      ge('clone_key_name').disabled = false;
      ge('move_key_name').disabled = false;

      var sectionPrefix = '';
      for (var i = 0; i < sectionsList.length; i++) {
        if (sectionsList[i][0] == newSectionId) {
          sectionPrefix = sectionsList[i][3];
          break;
        }
      }

      ge('action_clone_prefix').innerHTML = ge('action_move_prefix').innerHTML = sectionPrefix + "_";
      ge('clone_key_name').value = ge('move_key_name').value = key;

      ge('key_action_clone_button').onclick = translation.doCloneKey.pbind(key, newSectionId, sectionId, hash, false);
      ge('key_action_move_button').onclick = translation.doCloneKey.pbind(key, newSectionId, sectionId, hash, true);
    }

    new Dropdown(ge('key_actions'), [[-1, getLang('tran_choose_key_action')], [0, getLang('tran_delete_key')], [1, getLang('tran_copy_key')], [2, getLang('tran_move_key')]], {
      width: 160,
      selectedItems: '-1',
      multiselect: false,
      onChange: selectAction
    });

    var deleteButton = ge('key_action_delete_button');
    if (deleteButton) {
      deleteButton.onclick = translation.doDeleteKey.pbind(key, sectionId, hash);
    }
  },

  _toggleActionLoader: function (id, isShow) {
    this.replacedButtonsMap = this.replacedButtonsMap || {};
    if (isShow) {
      var buttonEl = ge(id);
      this.replacedButtonsMap[id] = buttonEl;
      var spinner = ce("img", { src: "/images/upload.gif", width:"32", height:"8", id: "actionProgressSpinned" });
      buttonEl.parentNode.replaceChild(spinner, buttonEl);
    } else {
      buttonEl = this.replacedButtonsMap[id];
      var spinner = ge('actionProgressSpinned');
      spinner.parentNode.replaceChild(buttonEl, spinner);
    }
  },

  doCloneKey: function(curKey, newSectionId, sectionId, hash, isMove) {
    var newKey = ge('clone_key_name').value;
    var self = _this;

    if (!newKey || this.saveInProgress) return;

    this.saveInProgress = true;
    hide('key-save-error');

    translation._toggleActionLoader('key_action_clone_button', true);
    translation._toggleActionLoader('key_action_move_button', true);

    ajax.post('al_translation.php', { act: 'clone_key', key: curKey, new_key: newKey, new_section_id: newSectionId, move: isMove, section_id: sectionId, hash: hash }, {
      onDone: function(error) {
        translation._toggleActionLoader('key_action_clone_button', false);
        translation._toggleActionLoader('key_action_move_button', false);
        if (error) {
          ge('key-save-error').innerHTML = error;
          show('key-save-error');
        }
      }
    });
  },

  doDeleteKey: function(key, sectionId, hash) {
    if (translation.saveInProgress) return false;
    translation.saveInProgress = true;

    hide('key-save-error');
    translation._toggleActionLoader('key_action_delete_button', true);

    var query = { act: 'delete_key', key: key, section_id: sectionId, hash: hash };
    ajax.post('al_translation.php', query, {
      onDone: function(error) {
        translation.saveInProgress = false;
        if (error) {
          ge('key-save-error').innerHTML = error;
          show('key-save-error');
        } else {
          curBox().hide();
          translation.selectSection(sectionId);
        }
      }
    });
  },

  _getSMSCount: function (text) {
    return SmsCounter.count(text).messages;
  },

  initLangChooser: function() {
    var langChooser = ge('lang-chooser');
    var items = [[1, 'Russian'], [3, 'English'], [4, 'Portuguese']];

    var classes = langChooser.classList;

    var itemsHTML = '<div class="mdd-item-selected-wrap"><div class="mdd-item-selected" id="' + items[0][0] + '">' + items[0][1] + '</div></div>';
    for (var i = 0; i < items.length; i++) {
      itemsHTML += '<div class="mdd-item" id="' + items[i][0] + '">' + items[i][1] + '</div>';
    }

    var ddEl = ce('div', { innerHTML:
      '<div class="mdd-value">' + items[0][1] + '</div>' +
      '<div class="mdd-items-wrap" style="display: none">' + itemsHTML + '</div>'
      , className: 'mdd-wrap', id: langChooser.id });

    langChooser.parentNode.replaceChild(ddEl, langChooser);

    var matches = function(el, selector) {
      return (el.matches || el.matchesSelector || el.msMatchesSelector || el.mozMatchesSelector || el.webkitMatchesSelector || el.oMatchesSelector).call(el, selector);
    };

    var itemsWrap = geByClass1('mdd-items-wrap', ddEl);

    addEvent(ddEl, 'click', function() {
      var valueEl = geByClass1('mdd-value', ddEl);
      show(itemsWrap);
      var size = getSize(valueEl);
      setStyle(itemsWrap, { 'margin-top': -size[1] - 6, 'margin-left': -11 });
    });
    setTimeout(function() {
      addEvent(window, 'click', function(event) {
        var node = event.target;
        var inDD = false;
        var limit = 100;
        while (limit-- && node != document) {
          if (matches(node, '.mdd-wrap')) {
            inDD = true;
            break;
          }
          node = node.parentNode;
        }

        if (!inDD) {
          hide(itemsWrap);
        }
      });
    }, 10);
  },

  go: function(el, ev) {
    var tabbedPaneEl = el.parentElement;
    var selectedTab = geByClass1('selected', tabbedPaneEl);
    removeClass(selectedTab, 'selected');

    addClass(el, 'selected');

    return nav.go(el, ev);
  },
}

window.t = translation.t;

try{stManager.done('translation.js');}catch(e){}

function AutoTokens(textarea, tokens) {
  var timeout, pattern = /{[^}]+}|%[a-z]/gi;
  addEvent(textarea, 'keyup', function() {
    clearTimeout(timeout);
    setTimeout(function(){
      onEnterText();
    }, 200);
  });
  each(tokens.childNodes, function(i, x){
    if (x.nodeType == 1) {
      addEvent(x, 'click', function(){return insertToken(x, x.innerHTML)});
    }
  });
  function onEnterText(e) {
    var matches = textarea.value.match(pattern);
    var index, token;
    each (tokens.childNodes, function(i, x) {
      if (x.nodeType != 1)
        return;
      token = x.innerHTML;
      index = matches != null ? indexOf(matches, token) : -1;
      if (index != -1) {
        matches.splice(index, 1);
        x.style.display = 'none';
      } else {
        x.style.display = '';
      }
    });
  }
  function insertToken(el, token) {
    if (textarea.selectionStart != textarea.selectionEnd) {
      textarea.value += token;
    } else {
      var sel = textarea.selectionStart + token.length;
      textarea.value = textarea.value.substring(0, textarea.selectionStart) + token + textarea.value.substr(textarea.selectionStart);
    }
    hide(el);
    if (sel > 0) {
      textarea.selectionStart = textarea.selectionEnd = sel;
    }
    textarea.focus();
    return false;
  }
}


(function() {
  var $, SmsCounter;

  window.SmsCounter = SmsCounter = (function() {
    function SmsCounter() {}

    SmsCounter.gsm7bitChars = "@£$¥èéùìòÇ\\nØø\\rÅåΔ_ΦΓΛΩΠΨΣΘΞÆæßÉ !\\\"#¤%&'()*+,-./0123456789:;<=>?¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ§¿abcdefghijklmnopqrstuvwxyzäöñüà";
    SmsCounter.gsm7bitExChar = "\\^{}\\\\\\[~\\]|€";
    SmsCounter.gsm7bitRegExp = RegExp("^[" + SmsCounter.gsm7bitChars + "]*$");
    SmsCounter.gsm7bitExRegExp = RegExp("^[" + SmsCounter.gsm7bitChars + SmsCounter.gsm7bitExChar + "]*$");
    SmsCounter.gsm7bitExOnlyRegExp = RegExp("^[\\" + SmsCounter.gsm7bitExChar + "]*$");
    SmsCounter.GSM_7BIT = 'GSM_7BIT';
    SmsCounter.GSM_7BIT_EX = 'GSM_7BIT_EX';
    SmsCounter.UTF16 = 'UTF16';
    SmsCounter.messageLength = {
      GSM_7BIT: 160,
      GSM_7BIT_EX: 160,
      UTF16: 70
    };
    SmsCounter.multiMessageLength = {
      GSM_7BIT: 153,
      GSM_7BIT_EX: 153,
      UTF16: 67
    };

    SmsCounter.count = function(text) {
      var count, encoding, length, messages, per_message, remaining;
      encoding = this.detectEncoding(text);
      length = text.length;
      if (encoding === this.GSM_7BIT_EX) {
        length += this.countGsm7bitEx(text);
      }
      per_message = this.messageLength[encoding];
      if (length > per_message) {
        per_message = this.multiMessageLength[encoding];
      }
      messages = Math.ceil(length / per_message);
      remaining = (per_message * messages) - length;
      return count = {
        encoding: encoding,
        length: length,
        per_message: per_message,
        remaining: remaining,
        messages: messages
      };
    };

    SmsCounter.detectEncoding = function(text) {
      switch (false) {
        case text.match(this.gsm7bitRegExp) == null:
          return this.GSM_7BIT;
        case text.match(this.gsm7bitExRegExp) == null:
          return this.GSM_7BIT_EX;
        default:
          return this.UTF16;
      }
    };

    SmsCounter.countGsm7bitEx = function(text) {
      var char2, chars;
      chars = (function() {
        var _i, _len, _results;
        _results = [];
        for (_i = 0, _len = text.length; _i < _len; _i++) {
          char2 = text[_i];
          if (char2.match(this.gsm7bitExOnlyRegExp) != null) {
            _results.push(char2);
          }
        }
        return _results;
      }).call(this);
      return chars.length;
    };

    return SmsCounter;

  })();

}).call(this);
