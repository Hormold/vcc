var ExchangeModer = {
  requestsInit: function() {
    each(geByClass('exchange_moder_request'), function(i, el) {
      var requestId = el.id.replace('exchange_moder_request', '');
      if (cur.uiReasonsControls[requestId]) {
        return;
      }

      placeholderSetup('moder_comment' + requestId);
      cur.uiReasonsControls[requestId] = function() {
        return new Ads.MultiDropdownMenu(cur.moderReasons, {
          target: ge('moder_reason_menu_' + requestId),
          columnsCount: 2
        });
      };
      cur.uiReasonsControlsInit[requestId] = function(elem) {
        elem.removeAttribute('onmouseover');
        cur.uiReasonsControls[requestId] = cur.uiReasonsControls[requestId]();
      };

      var realSize = getSize(geByClass1('wall_post_text', el).parentNode)[1];
      if (realSize < 185) {
        var post_wrap = geByClass1('exchange_preview_short', el);
        if (post_wrap) {
          removeClass(post_wrap, 'exchange_preview_short');
        }
      }
    });
  },

  processRequestWithConfitmation: function(action, requestId) {
    function executeProcessRequest() {
      ExchangeModer.processRequest(action, requestId);
      msgBov.hide();
    }
    var msgBov = showFastBox('�������������', '�� �������? ��� ������ ����� ��������!', '��, �������� � ��������', executeProcessRequest, getLang('box_cancel'));

  },
  processRequest: function(action, requestId) {
    var requestParams = cur.requestsParams[requestId];
    if (!requestId || !requestParams) {
      return;
    }
    var resultArea = ge('moder_request_result_area' + requestId);

    var params = extend(requestParams, {
      action: action,
      comment: val('moder_comment' + requestId)
    });
    if ((action === 'disapprove') || (action === 'force_disapprove')) {
      params.moder_rules = cur.uiReasonsControls[requestId].getSelectedItems().join(',');
    }

    ajax.post('/exchangemoder?act=a_process_request', params, {
      onDone: onComplete.pbind(false),
      onFail: onComplete.pbind(true),
      showProgress: function () {
        resultArea.innerHTML = '<img src="/images/upload.gif" />';
      }
    });

    if (cur.processRequestLock) {
      return;
    }
    cur.processRequestLock = true;

    function onComplete(isError, response) {
      cur.processRequestLock = false;
      var msg = response.msg || '';
      if (!msg) {
        isError = true;
        msg = '������!';
      } else if (response.error) {
        isError = true;
      }
      resultArea.innerHTML = msg;
      if (isError) {
        removeClass(resultArea, 'exchange_moder_request_result_success');
        addClass(resultArea, 'exchange_moder_request_result_error');
      } else {
        removeClass(resultArea, 'exchange_moder_request_result_error');
        addClass(resultArea, 'exchange_moder_request_result_success');
      }
      if (!response.tmp_error && nav.objLoc.requests) {
        fadeOut('exchange_moder_request' + requestId, 500);
      }
      return true;
    }
  },
  showRequestInfo: function(requestId) {
    showBox('/exchangemoder?act=a_request_info_box', {request_id: requestId}, {dark: 1});
  },
  loadRequests: function(type, btn) {
    var params = {requests: type, load: 1};
    var rq = geByClass('exchange_moder_request', 'exchange_moder_requests_container');

    if (!type || type == '') {
      params['last_id'] = rq[rq.length - 1].id.replace('exchange_moder_request', '');
    } else {
      params['cur_ids'] = [];
      for (i in rq) {
        params['cur_ids'].push(rq[i].id.replace('exchange_moder_request', ''));
      }
      params['cur_ids'] = params['cur_ids'].join(',');
    }
    ajax.post('/exchangemoder', params, {
      onDone: function(response, js) {
        if (!isObject(response)) {
          ge('ads_page').innerHTML = response;
          eval('(function(){' + js + ';})()');
          return;
        }

        if (response.requests) {
          ge('exchange_moder_requests_container').innerHTML += response.requests;
        }
        if (response.work_stopped) {
          ge('exchange_moder_requests_container').innerHTML = '';
          cur.requestsParams = {};
          cur.uiReasonsControls = {};
          if (cur.checkNewRequestsInt) {
            clearInterval(cur.checkNewRequestsInt);
            cur.checkNewRequestsInt = false;
          }
        }
        if (response.button) {
          ge('exchange_moder_button_container').innerHTML = response.button;
        }
        ge('exchange_moder_summary').innerHTML = response.summary;
        if (js) {
          eval('(function(){' + js + ';})()');
          ExchangeModer.requestsInit();
        }

        if (type == 'work_start') {
          cur.checkNewRequestsInt = setInterval(ExchangeModer.checkNewRequests, 5000);
        }
      },
      showProgress: function() {
        if (btn) lockButton(btn);
      },
      hideProgress: function() {
        if (btn) unlockButton(btn);
      }
    });
  },
  checkNewRequests: function() {
    ExchangeModer.loadRequests('work');
  },

  showFullPost: function(id) {
    var elems = ['exchange_moder_post'+id, 'exchange_moder_last_post'+id];
    for (var i in elems) {
      var el = elems[i];
      if (!ge(el)) continue;

      var post = geByClass1('exchange_post_msg', el);
      if (!post) {
        return;
      }

      removeClass(post.parentNode, 'exchange_preview_short');
      setStyle(post, {maxHeight: 'none'});
    }
  },
  slideFullPost: function(id) {
    var elems = ['exchange_moder_post'+id, 'exchange_moder_last_post'+id];
    for (var i in elems) {
      var el = elems[i];
      if (!ge(el)) continue;

      var post = geByClass1('exchange_post_msg', el);
      var more = geByClass1('exchange_post_msg_more', el);
      if (!post || !more) {
        return;
      }

      var realSize = getSize(geByClass1('wall_post_text', post).parentNode)[1];
      animate(post, {maxHeight: realSize}, 200, ExchangeModer.showFullPost.pbind(id));
      animate(more, {height: 0}, 200);
    }
  }
};

try{stManager.done('exchange_moder.js');}catch(e){}
